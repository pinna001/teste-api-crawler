# Projeto teste de seleção

## Instruções instalação
* git clone https://pinna001@bitbucket.org/pinna001/teste-api-crawler.git
* composer install
* cp .env.example .env

## Instruções de uso

### Lista:
```json
{
    Method: POST,
    URL: /api/lista,
    Header: {
        http-x-api-key : senhaDoAPI
    },
    Data: {
        tipo_veiculo : [carro,moto,caminhao],
        marca: [volkswagem,fiat,....],
        modelo: [gol, golf, passat,...],
        ano_min: yyyy,
        ano_max: yyyy,
        preco_min: 0-9+,
        preco_max: 0-9+,
        km_min: 0-9+,
        km_max: 0-9+,
        zero_usado: [zero,usado],
        revenda_particular: [revenda,particular]        
    }
}
```

### Detalhes:
```json
{
    Method: POST,
    URL: /api/detalhes,
    Header: {
        http-x-api-key: senhaDoAPI
    },
    Data: {
        link: "Campo link do endPoint Lista"
    }
}
```
